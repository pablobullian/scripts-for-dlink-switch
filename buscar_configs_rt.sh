#!/bin/bash
SERVER=192.168.0.202
PATH_TFTP=config_routers
i=0
j=0
while read line; do
if [[ "$line" =~ ^[^#]*= ]]; then
	tipo=`echo $line | cut -d'=' -f 1`
	value=`echo $line | cut -d'=' -f 2-`
        
	if [ $tipo == "IP" ];then
		((i++))
		ip[i]=$value
	fi
	if [ $tipo == "NOMBRE" ];then
			((j++))
		nombre[j]=$value
	fi
fi
done < routers.config

#echo "hasta aca $i"
#SERVER=192.168.0.202
while (( $i != 0 ))
do
	echo "\n Copiando ${nombre[i]} \n"
	ssh admin@${ip[i]} "exec vbash -i -c 'show configuration'" > /tftp_storage/$PATH_TFTP/${nombre[i]}.conf
	((i--))
done
echo "salio"
